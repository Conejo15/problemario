<?php
fscanf(STDIN, "%s", $server);
fscanf(STDIN, "%s", $user);
fscanf(STDIN, "%s", $pass);
fscanf(STDIN, "%s", $db);

$conn = mysqli_connect($server, $user, $pass, $db);


if ($conn) {
  $query = "SELECT DISTINCT S.Nombre                                       AS name,
  (SELECT FORMAT(IFNULL(SUM(f.Monto), 0), 2)
      FROM BD_PagoServ_Facturas f JOIN BD_PagoServ_Servicios S ON f.id_Servicio = S.id
      WHERE S.Nombre = name
        AND f.fecha_Pago <= f.fecha_Vencimiento) AS total,
  (SELECT COUNT(*)
      FROM BD_PagoServ_Facturas f JOIN BD_PagoServ_Servicios S ON f.id_Servicio = S.id
      WHERE S.Nombre = name)                    AS qty
FROM BD_PagoServ_Facturas f RIGHT OUTER JOIN BD_PagoServ_Servicios S ON f.id_Servicio = S.id
HAVING qty > 0
ORDER BY name  ASC,
total DESC;";

  $data = mysqli_fetch_all(mysqli_query($conn, $query), MYSQLI_ASSOC);

  foreach ($data as $service)
    fwrite(STDOUT, utf8_encode($service['name'] . ':' . $service['qty'] . ':$' . $service['total'] . PHP_EOL));
}
