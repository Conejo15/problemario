 Un polígono interesante es simplemente un cuadrado con un lado de longitud 1. Un polígono interesante de mayor grado se obtiene al agregar más  polígonos interesantes en sus lados como se muestra en la figura se tienen polígonos de grado 1, 2, 3 y 4.

Entrada

    El número n  de casos a evaluar, seguido de los n números que representan cada caso.

Salida

  Para cada caso se deberá mostrar el área del polígono interesante.

Ejemplo

Entrada	
2
6
2

Salida
61
5



