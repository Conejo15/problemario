<?php
fscanf(STDIN, "%i %i %i %i", $a, $b, $c, $d);

if ($b > $a)
	$first = 0;
else
	$first = $a == 0 ? 0 : round((($a - $b) * 100) / $a, 2);

if ($d > $c)
	$second = 0;
else
	$second = $c == 0 ? 0 : round((($c - $d) * 100) / $c, 2);

fwrite(STDOUT, "$first% $second%" . PHP_EOL);
