<?php
fscanf(STDIN, "%s", $s);
fscanf(STDIN, "%s", $u);
fscanf(STDIN, "%s", $p);
fscanf(STDIN, "%s", $d);

$link = mysqli_connect($s, $u, $p, $d);

if ($link)
{
    $query = "SELECT DISTINCT tco.TABLE_NAME
            FROM information_schema.table_constraints tco
            WHERE tco.table_schema = '$d'
            ORDER BY tco.table_name DESC;";

    $tables = mysqli_fetch_all(mysqli_query($link, $query), MYSQLI_NUM);

    $res = "";
    foreach ($tables as $table)
        $res .= $table[0].":";

    fwrite(STDOUT, substr($res, 0, strlen($res) - 1) . PHP_EOL);
}