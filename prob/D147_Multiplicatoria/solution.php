<?php
function fillArray($start, $size)
{
	$result = [];
	$j = 0;
	for ($i = $start; $i < ($start + $size); $i++) {
		$result[$j] = $i;
		$j++;
	}

	return $result;
}

fscanf(STDIN, "%i", $n);

for ($i = 0; $i < $n; $i++) {
	fscanf(STDIN, "%i %i %i %i %i", $x, $y, $z, $size, $goal);

	$a = fillArray($x, $size);
	$b = fillArray($y, $size);
	$c = fillArray($z, $size);

	$result = 0;

	foreach ($a as $v1) {
		$aux1 = $goal;
		if ($aux1 % $v1 == 0) {
			$aux2 = $aux1 / $v1;
			foreach ($b as $key => $v2)
				if ($aux2 % $v2 == 0) {
					$aux3 = $aux2 / $v2;
					foreach ($c as $v3)
						if ($aux3 == $v3)
							$result++;
				}
		}
	}

	fwrite(STDOUT, "$result" . PHP_EOL);
}
