<?php
$url = 'http://www.genware.es/xValTarjetaCredito.php';

while ($card = trim(fgets(STDIN))) {
	$data = array('tarjeta' => $card);

	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	if ($result == TRUE)
		fwrite(STDOUT, ((explode("|", $result)[0]) ? "VALIDO" : "NO valido") . PHP_EOL);
}
