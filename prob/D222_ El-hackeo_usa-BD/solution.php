<?php
fscanf(STDIN, "%s %s %s %s", $s, $u, $p, $d);

$link = mysqli_connect($s, $u, $p, $d);

while ($x = trim(fgets(STDIN))) {
    $x = explode(" ", $x);

    $query = "SELECT u.Usuario, u.Nombre, u.Apellidos, u.Clave, f.Ref_Bancaria
                FROM BD_PagoServ_Tipo_Pago t JOIN BD_PagoServ_Facturas f ON t.id = f.id_FormaPago
                                 JOIN Usuarios u ON f.id_Cliente = u.Usuario
                WHERE t.Nombre = 'Tarjeta'
                  AND u.Usuario = '" . $x[0] . "'
                  AND u.Clave = PASSWORD('" . $x[1] . "')
              ORDER BY f.Ref_Bancaria ASC;";


    $data = mysqli_fetch_all(mysqli_query($link, $query), MYSQLI_ASSOC);

    if ($data) {
        $result = utf8_encode($data[0]['Usuario']) . ":" . utf8_encode($data[0]['Nombre'] . ' ' . $data[0]['Apellidos']);

        foreach ($data as $x)
            $result .= ':' . $x['Ref_Bancaria'];

        fwrite(STDOUT, $result . PHP_EOL);
    }
}