<?php
function isValid($isbn)
{
	$value = 0;

	for ($i = 0; $i < 13; $i += 2)
		$value += (int)$isbn[$i];

	for ($i = 1; $i < 12; $i += 2)
		$value += 3 * $isbn[$i];

	return (0 === ($value % 10)) ? true : false;
}

fscanf(STDIN, "%i", $n);

while ($isbn = trim(fgets(STDIN))) {
	$isbn = preg_replace('/\D/', '', $isbn);

	$result = "INCORRECTO";
	if (strlen($isbn) == 13)
		if (isValid($isbn))
			$result = "CORRECTO";
	fwrite(STDOUT, "$result" . PHP_EOL);
}
