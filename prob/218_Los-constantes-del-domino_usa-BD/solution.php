<?php
fscanf(STDIN, "%s", $server);
fscanf(STDIN, "%s", $user);
fscanf(STDIN, "%s", $pass);
fscanf(STDIN, "%s", $db);

$conn = mysqli_connect($server, $user, $pass, $db);

if ($conn) {
	$user = "SELECT COUNT(*) AS usuario, U.Nombre, U.Apellidos
				FROM BD_Domino_Juegos J JOIN Usuarios U ON J.id_invitado = U.Usuario
				GROUP BY id_usuario
				HAVING usuario = (
					SELECT MAX(y.count) AS count
						FROM (SELECT COUNT(*) AS count
								FROM BD_Domino_Juegos
								GROUP BY id_usuario) y)
				ORDER BY U.Apellidos ASC;";
	$guest = "SELECT COUNT(*) AS usuario, U.Nombre, U.Apellidos
				FROM BD_Domino_Juegos J JOIN Usuarios U ON J.id_invitado = U.Usuario
				GROUP BY id_invitado
				HAVING usuario = (
					SELECT MAX(y.count) AS count
						FROM (SELECT COUNT(*) AS count
								FROM BD_Domino_Juegos
								GROUP BY id_invitado) y)
				ORDER BY U.Apellidos ASC;";

	$userData = mysqli_fetch_all(mysqli_query($conn, $user), MYSQLI_ASSOC);
	$guestData = mysqli_fetch_all(mysqli_query($conn, $guest), MYSQLI_ASSOC);

	fwrite(STDOUT, "Invita" . PHP_EOL);

	foreach ($userData as $value)
		fwrite(STDOUT, utf8_encode($value['Nombre']) . " " . utf8_encode($value['Apellidos']) . PHP_EOL);

	fwrite(STDOUT, "Invitado" . PHP_EOL);

	foreach ($guestData as $value)
		fwrite(STDOUT, utf8_encode($value['Nombre']) . " " . utf8_encode($value['Apellidos']) . PHP_EOL);
}
