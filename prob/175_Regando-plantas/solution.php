<?php
fscanf(STDIN, "%i", $n);

for ($i = 0; $i < $n; $i++)
{
	fscanf(STDIN, "%i %i %i", $x, $y, $a);

	if ($y - $x >= 0)
		fwrite(STDOUT, "0" . PHP_EOL);
	else
		fwrite(STDOUT, ceil($a / ($x - $y)) . PHP_EOL);
}
