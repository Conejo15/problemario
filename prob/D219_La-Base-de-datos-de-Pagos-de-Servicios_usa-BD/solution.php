<?php
fscanf(STDIN, "%s", $s);
fscanf(STDIN, "%s", $u);
fscanf(STDIN, "%s", $p);
fscanf(STDIN, "%s", $d);

$link = mysqli_connect($s, $u, $p, $d);
$TN = "BD_PagoServ_Facturas";

$foreignQuery = "SELECT k.COLUMN_NAME            AS FoFi,
                        k.REFERENCED_TABLE_NAME  AS FoTa,
                        k.REFERENCED_COLUMN_NAME AS PrFi,
                        c.COLUMN_TYPE            AS CoTi
                
                FROM information_schema.TABLE_CONSTRAINTS i
                LEFT JOIN information_schema.KEY_COLUMN_USAGE k 
                ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME
                JOIN information_schema.COLUMNS c 
                ON k.COLUMN_NAME = c.COLUMN_NAME
                WHERE i.CONSTRAINT_TYPE = 'FOREIGN KEY'
                AND i.TABLE_SCHEMA = '$d'
                AND i.TABLE_NAME = '$TN'
                ORDER BY FoFi;";

$primaryQuery = "SELECT kcu.COLUMN_NAME, c.COLUMN_TYPE
                  FROM information_schema.table_constraints tco
        
                  JOIN information_schema.key_column_usage kcu
                  ON tco.constraint_schema = kcu.constraint_schema
                    AND tco.constraint_name = kcu.constraint_name
                    AND tco.table_name = kcu.table_name
                  JOIN information_schema.COLUMNS c
                  ON kcu.COLUMN_NAME = c.COLUMN_NAME
                    AND kcu.TABLE_SCHEMA = c.TABLE_SCHEMA
                    AND kcu.TABLE_NAME = c.TABLE_NAME
                  WHERE tco.constraint_type = 'PRIMARY KEY'
                    AND tco.table_schema = '$d'
                    AND tco.TABLE_NAME = '$TN'
                  ORDER BY tco.table_schema,tco.table_name,kcu.ordinal_position;";

if ($link)
{
    $foreignData = mysqli_fetch_all(mysqli_query($link, $foreignQuery), MYSQLI_ASSOC);
    $primaryData = mysqli_fetch_assoc(mysqli_query($link, $primaryQuery));

    fwrite(STDOUT, "Nombre de llave primaria: " . $primaryData['COLUMN_NAME'] . " [" . $primaryData['COLUMN_TYPE'] . "]" . PHP_EOL);
    fwrite(STDOUT, "Foraneas:" . PHP_EOL);

    foreach ($foreignData as $table)
        fwrite(STDOUT, "Nombre:" . $table['FoFi'] .  " <=> Tabla Referenciada:" . $table['FoTa'] . " <=> CampoForaneo:" . $table['PrFi'] . " <=> [" . $table['CoTi'] . "]" . PHP_EOL);
}
?>