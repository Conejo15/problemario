    <?php
    function hasDuplicates($sequence) {
        $aux = explode(" ", $sequence);

        for ($j = 0; $j < sizeof($aux); $j++)
            for ($i = 0; $i < sizeof($aux); $i++)
                if ($i != $j && ($aux[$j] == $aux[$i] || strrev($aux[$j]) == $aux[$i]))
                    return true;

        return false;
    }

    function ifHasBadSequence($sequence) {
        $aux = explode(" ", $sequence);

        for ($i = 0; $i < sizeof($aux) - 1; $i++)
            if ($aux[$i][2] != $aux[$i + 1][0])
                return true;

        return false;
    }

    fscanf(STDIN, "%s", $server);
    fscanf(STDIN, "%s", $user);
    fscanf(STDIN, "%s", $pass);
    fscanf(STDIN, "%s", $db);

    $conn = mysqli_connect($server, $user, $pass, $db);

    if (!$conn) {
        echo "Error";
    } else {
        $query = "SELECT id, secuencia, id_usuario, id_invitado FROM Juegos WHERE id_estatus != 2";
        $result = mysqli_fetch_all(mysqli_query($conn, $query), MYSQLI_ASSOC);
        $error = "";

        foreach ($result as $row) {
            if(ifHasBadSequence($row['secuencia']))
                $error = "Secuencia Mal";
            if(hasDuplicates($row['secuencia']))
                $error = "Ficha Duplicada";

            if (strlen($error) > 0) {
                $userQuery = "SELECT Nombre, Apellidos FROM Usuarios WHERE Usuario = '" . $row['id_usuario'] . "'";
                $userData = mysqli_fetch_assoc(mysqli_query($conn, $userQuery));
                $user = utf8_encode($userData["Nombre"] . " " . $userData["Apellidos"]);

                $guestQuery = "SELECT Nombre, Apellidos FROM Usuarios WHERE Usuario = '" . $row['id_invitado'] . "'";
                $guestData = mysqli_fetch_assoc(mysqli_query($conn, $guestQuery));
                $guest = utf8_encode($guestData["Nombre"] . " " . $guestData["Apellidos"]);

                $message = $row['id'] . ":" . $user . ":" . $guest . ":" . $error;

                fwrite(STDOUT, $message . PHP_EOL);
            }

            $error = "";
        }
    }

    mysqli_close($conn);