<?php
function is_valid($email) {
	$result = filter_var($email, FILTER_VALIDATE_EMAIL);

	if ($result)
		return explode('@', $result)[1];
	else
		return "DOMINIO INCORRECTO";
}

fscanf(STDIN, "%i", $n);

for ($i = 0; $i < $n; $i++) {
	fscanf(STDIN, "%s", $email);
	fwrite(STDOUT, (is_valid($email) . PHP_EOL));
}