//225-tarjetas plasticas
<?php
function validar($card)
{
    $result = 0;
    for ($i = 0; $i < strlen($card); $i++) {
        $num = (int)$card[$i];

        if ($i % 2 == 0) {
            $num *= 2;
            $result +=($num > 9)?($num % 10) + (int)($num / 10):$num;    
        }
        else
            $result += $num;    
    }

    return $result;
}

fscanf(STDIN, "%i", $n);

for ($i=0; $i < $n; $i++)
{ 
    fscanf(STDIN, "%s", $card);
    $val = validar($card);
    $result = ($val % 10) === 0 ? 'VALIDA' : 'NO Valida';
    fwrite(STDOUT, ("$result" . PHP_EOL));
}
?>