<?php
fscanf(STDIN, "%s", $n);

for ($i = 0; $i < $n; $i++) {
	fscanf(STDIN, "%[^n]", $line);
	$tokens = explode(" ", $line);

	$result = "";

	$count = 0;
	$fills = 0;
	for ($i = 1; $i < sizeof($tokens); $i++) {
		$fills++;
		$count += $tokens[$i];

		if ($count >= 100) {
			$result .= "$fills ";
			$fills = 0;
			$count -= 100;
		}
	}

	fwrite(STDOUT, trim($result) . PHP_EOL);
}
