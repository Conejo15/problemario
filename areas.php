<?php
fscanf(STDIN, "%i", $n);

for ($j = 0 ; $j < $n; $j++) {
	fscanf(STDIN, "%i", $x);
	$result = 1;

	for ($i = 1; $i <= $x; $i++)
		$result += ($i * 4) - 4;

	fwrite(STDOUT, ($result). PHP_EOL);
}